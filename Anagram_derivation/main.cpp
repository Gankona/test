#include <QApplication>
#include "mainwidget.h"
#include "test/test_anagrammanager.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    MainWidget wgt;
    wgt.setWindowTitle("Anagram derivation");
    wgt.setMinimumSize(600, 400);
    wgt.show();

    Test_AnagramManager testAnagram;
    QTest::qExec(&testAnagram);

    return app.exec();
}
