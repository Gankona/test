QT += widgets
QT += testlib
QT += gui

CONFIG += c++14

HEADERS += \
    mainwidget.h \
    anagrammanager.h \
    test/test_anagrammanager.h

SOURCES += \
    mainwidget.cpp \
    main.cpp \
    anagrammanager.cpp \
    test/test_anagrammanager.cpp
