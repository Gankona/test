#include "anagrammanager.h"

AnagramManager::AnagramManager(QObject *parent)
    : QObject(parent)
{}

void AnagramManager::findLongestAnagram(QString firstWord)
{
    if (firstWord.length() != 3)
        return;
    /// if word lenght is 3, we send it to function,
    /// where recursively scans and finds the longest string and emit this to widget
    emit setLongestAnagram(findNextWordDerivation(firstWord.toLower()));
}

QStringList AnagramManager::findNextWordDerivation(QString word)
{
    QList <QStringList> list;
    /// find all derivation by input word, iterate throw all the values
    /// that lenght at 1 and test difference with current input word
    /// if we have 1 difference, find all derivation by it word
    for (QString s : currentDictionary.values(word.length() + 1))
        if (AnagramManager::countWordsDifference(word, s) == 1)
            list += findNextWordDerivation(s);

    // looking for the longest list
    int maxIndex(-1);
    int maxLenght(-1);
    for (int i = 0; i < list.length(); i++)
        if (list.at(i).length() > maxLenght){
            maxLenght = list.at(i).length();
            maxIndex = i;
        }

    // return input word with most longest list
    QStringList ret(word);
    if (maxIndex >= 0)
        ret += list.at(maxIndex);
    return ret;
}

// return count difference between two words
int AnagramManager::countWordsDifference(QString first, QString last)
{
    int dismatchCount(0);
    for (int i = 0; i < last.length(); i++)
        dismatchCount += abs(first.count(last[i]) - last.count(last[i]));
    return dismatchCount;
}

// return difference char between first and last, if multiple difference, return first char
QChar AnagramManager::getDifferenceChar(QString first, QString last)
{
    for (int i = 0; i < last.length(); i++)
        if (first.count(last[i]) != last.count(last[i]))
            return last[i];
    return QChar();
}

void AnagramManager::addWordToDictionary(QString s)
{
    // add word to dictionary to first non latin char or and of word
    s = s.toLower();
    QString addString("");
    for (int i = 0; i < s.length(); i++){
        if (s[i] < 'a' && s[i] > 'z')
            break;
        else
            addString += s[i];
    }
    if (addString.length() < 4)
        return;
    currentDictionary.insert(addString.length(), addString);
}

void AnagramManager::setStandartDictionary()
{
    currentDictionary.clear();
    addWordToDictionary("tennis");
    addWordToDictionary("nails");
    addWordToDictionary("desk");
    addWordToDictionary("aliens");
    addWordToDictionary("table");
    addWordToDictionary("engine");
    addWordToDictionary("sail");
}

std::pair<bool ,QString> AnagramManager::loadDictionary(QString fileFullName)
{
    QFile file(fileFullName);
    if (file.open(QIODevice::ReadOnly)){
        // read dictionary
        currentDictionary.clear();
        QTextStream stream(&file);
        while (! stream.atEnd())
            addWordToDictionary(stream.readLine());
        file.close();
        return std::make_pair(true, file.fileName().split('/').last());
    }
    return std::make_pair(false, file.errorString());
}
