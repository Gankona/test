#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include "anagrammanager.h"

class MainWidget : public QWidget
{
    Q_OBJECT
private:
    QLineEdit *inputLineEdit;
    QLabel *currentDictionaryLabel;
    QLabel *resultLabel;
    QPushButton *setDefaultButton;
    QPushButton *openDictionaryButton;

    QHBoxLayout *topPanelLayout;
    QVBoxLayout *mainLayout;

    AnagramManager *anagramManager;

    void tryOpenDictionary();
    void setLongestAnagram(QStringList list);

public:
    explicit MainWidget(QWidget *parent = 0);
};

#endif // MAINWIDGET_H
