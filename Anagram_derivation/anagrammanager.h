#ifndef ANAGRAMMANAGER_H
#define ANAGRAMMANAGER_H

#include <QtCore>

class AnagramManager : public QObject
{
    Q_OBJECT
private:
    QMultiMap <int/*char count*/, QString> currentDictionary;

    inline void addWordToDictionary(QString s);
    QStringList findNextWordDerivation(QString word);

public:
    AnagramManager(QObject *parent = nullptr);

    void setStandartDictionary();
    std::pair<bool, QString> loadDictionary(QString fileFullName);
    void findLongestAnagram(QString firstWord);

    static QChar getDifferenceChar(QString first, QString last);
    static int countWordsDifference(QString first, QString last);

signals:
    void setLongestAnagram(QStringList list);
};

#endif // ANAGRAMMANAGER_H
