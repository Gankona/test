#include "test_anagrammanager.h"

Test_AnagramManager::Test_AnagramManager(QObject *parent)
    : QObject(parent)
{

}

void Test_AnagramManager::testDifferenceChar()
{
    AnagramManager manager;
    QCOMPARE(manager.getDifferenceChar("qwer", "qwe"), QChar());
    QCOMPARE(manager.getDifferenceChar("qwe", "qwer"), QChar('r'));
}

void Test_AnagramManager::testCountWordsDifference()
{
    AnagramManager manager;
    QCOMPARE(manager.countWordsDifference("qwert", "qwer"), 0);
    QCOMPARE(manager.countWordsDifference("qwer", "qwert"), 1);
    QCOMPARE(manager.countWordsDifference("qwer", "qwer"), 0);
}
