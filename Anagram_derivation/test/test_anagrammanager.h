#ifndef TEST_ANAGRAMMANAGER_H
#define TEST_ANAGRAMMANAGER_H

#include <QtCore>
#include <QtGui>
#include <QtTest>

#include "anagrammanager.h"

class Test_AnagramManager : public QObject
{
    Q_OBJECT
public:
    explicit Test_AnagramManager(QObject *parent = 0);

private slots:
    void testDifferenceChar();
    void testCountWordsDifference();
};

#endif // TEST_ANAGRAMMANAGER_H
