#include "mainwidget.h"

MainWidget::MainWidget(QWidget *parent) : QWidget(parent)
{
    QRegExp rx("[A-Z,a-z]{0,3}"); // three simbols content whatever latin char
    QValidator *validator = new QRegExpValidator(rx, this);

    inputLineEdit = new QLineEdit;
    inputLineEdit->setValidator(validator);

    inputLineEdit->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    resultLabel = new QLabel;
    resultLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    resultLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    currentDictionaryLabel = new QLabel;
    currentDictionaryLabel->setMargin(3);
    currentDictionaryLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum);
    setDefaultButton = new QPushButton("Standart dictionary");
    setDefaultButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    openDictionaryButton = new QPushButton("Load dictionary");
    openDictionaryButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    QObject::connect(openDictionaryButton, &QPushButton::clicked, this, &MainWidget::tryOpenDictionary);

    topPanelLayout = new QHBoxLayout;
    topPanelLayout->addWidget(setDefaultButton);
    topPanelLayout->addWidget(openDictionaryButton);
    topPanelLayout->addWidget(currentDictionaryLabel);

    mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(topPanelLayout);
    mainLayout->addWidget(inputLineEdit);
    mainLayout->addWidget(resultLabel);

    anagramManager = new AnagramManager(this);

    QObject::connect(anagramManager, &AnagramManager::setLongestAnagram, this, &MainWidget::setLongestAnagram);

    QObject::connect(inputLineEdit, &QLineEdit::textChanged, [=]()
    {
        if (inputLineEdit->text().length() == 3)
            anagramManager->findLongestAnagram(inputLineEdit->text());
        else
            resultLabel->clear();
    });

    QObject::connect(setDefaultButton, &QPushButton::clicked, [=]()
    {
        currentDictionaryLabel->setText("Standart dictionary");
        anagramManager->setStandartDictionary();
        anagramManager->findLongestAnagram(inputLineEdit->text());
    });

    setDefaultButton->click();
    inputLineEdit->setFocus();
}

void MainWidget::tryOpenDictionary()
{
    QFileDialog dialog(this, "Open file", QDir::currentPath(), "*.txt");
    QObject::connect(&dialog, &QFileDialog::fileSelected, [=](QString fileName)
    {
        auto result = anagramManager->loadDictionary(fileName);
        // successfull load the dictionary
        if (result.first){
            currentDictionaryLabel->setText("External dictionary: " + result.second);
            anagramManager->findLongestAnagram(inputLineEdit->text());
        }
        else {
            // show error message
            QMessageBox errorBox;
            errorBox.setText("Cannot read the file\n" + result.second);
            errorBox.setIcon(QMessageBox::Warning);
            errorBox.exec();
        }
    });
    dialog.exec();
}

void MainWidget::setLongestAnagram(QStringList list)
{
    QString lastWord = inputLineEdit->text();
    QString resultText("Anagram derivation by \"" + lastWord + "\" word:\n" + lastWord);
    for (QString s : list){
        // find dismatch char between current and last word
        for (int i = 0; i < s.length(); i++){
            bool isMatch(false);
            for (int j = 0; j < lastWord.length(); j++)
                if (lastWord[j] == s[i])
                    isMatch = true;
            if (! isMatch){
                resultText += (" + " + s[i] + " = " + s);
                break;
            }
        }
    }
    resultLabel->setText(resultText);
}
