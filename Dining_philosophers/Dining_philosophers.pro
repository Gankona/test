QT += qml quick network
CONFIG += c++14

SOURCES += \
    main.cpp \
    C++/mainmanager.cpp

RESOURCES += \
    qml.qrc

HEADERS += \
    C++/mainmanager.h

OTHER_FILES += \
    QML/*.qml
