#include <QGuiApplication>
#include "C++/mainmanager.h"

int main(int argc, char** argv)
{
    QGuiApplication app(argc, argv);

    MainManager *manager = new MainManager;

    return app.exec();
}
