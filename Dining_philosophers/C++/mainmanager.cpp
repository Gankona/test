#include "mainmanager.h"

MainManager::MainManager(QObject *parent) : QObject(parent)
{
    view = new QQuickView();
    view->rootContext()->setContextProperty("Qt_match", this);
    view->setSource(QUrl("qrc:/QML/main.qml"));
    view->resize(600, 600);
    view->show();
}
