#ifndef MAINMANAGER_H
#define MAINMANAGER_H

#include <QtCore>
#include <QtGui>
#include <QQuickView>
#include <QQmlContext>
#include <QQuickItem>

class MainManager : public QObject
{
    Q_OBJECT
private:
    QQuickView *view;

public:
    explicit MainManager(QObject *parent = 0);

signals:

public slots:
};

#endif // MAINMANAGER_H
