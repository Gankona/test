#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
#include "morsetranslator.h"
#include <QStyle>
#include <memory>

class MainWidget : public QWidget
{
    Q_OBJECT
private:
    InputType inputType;
    QString morseCaption = "Morse code";
    QString latinCaption = "Latin text";

    QPushButton *clearButton;
    QPushButton *saveToFileButton;
    QPushButton *openFileButton;
    QPushButton *swapModeButton;
    QLabel *filePathLabel;
    QLabel *resultTranslatorLabel;
    QLabel *signInputCaptionLabel;
    QLabel *signOutputCaptionLabel;
    QTextEdit *inputTextEdit;

    QHBoxLayout *menuLayout;
    QHBoxLayout *captionLayout;
    QHBoxLayout *inputOutputLayout;
    QVBoxLayout *mainLayout;

    MorseTranslator *morseTranslator;

    void swapInputType();
    void validatingInput();

    void openFileDialog();

protected:
    void keyPressEvent(QKeyEvent *event);

public:
    explicit MainWidget(QWidget *parent = nullptr);

    void clear();
};

#endif // MAINWIDGET_H
