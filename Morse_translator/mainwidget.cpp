#include "mainwidget.h"

MainWidget::MainWidget(QWidget *parent) : QWidget(parent)
{
    // menu frame
    clearButton = new QPushButton(style()->standardIcon(QStyle::SP_DialogResetButton), " clear", this);
    clearButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
    QObject::connect(clearButton, &QPushButton::clicked, this, &MainWidget::clear);
    openFileButton = new QPushButton(style()->standardIcon(QStyle::SP_DialogOpenButton), " open file", this);
    openFileButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
    QObject::connect(openFileButton, &QPushButton::clicked, this, &MainWidget::openFileDialog);
    saveToFileButton = new QPushButton(style()->standardIcon(QStyle::SP_DialogSaveButton), " save", this);
    saveToFileButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);

    // caption frame
    swapModeButton = new QPushButton("< >", this);
    swapModeButton->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Expanding);
    QObject::connect(swapModeButton, &QPushButton::clicked, [=](){ swapInputType(); });
    filePathLabel = new QLabel(this);
    filePathLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    signInputCaptionLabel = new QLabel(this);
    signOutputCaptionLabel = new QLabel(this);
    signOutputCaptionLabel->setAlignment(Qt::AlignRight);

    resultTranslatorLabel = new QLabel(this);
    resultTranslatorLabel->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    resultTranslatorLabel->setFrameShape(QFrame::StyledPanel);
    resultTranslatorLabel->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    resultTranslatorLabel->setWordWrap(true);
    inputTextEdit = new QTextEdit(this);
    inputTextEdit->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    menuLayout = new QHBoxLayout;
    menuLayout->addWidget(clearButton);
    menuLayout->addWidget(saveToFileButton);
    menuLayout->addWidget(filePathLabel);
    menuLayout->addWidget(openFileButton);

    captionLayout = new QHBoxLayout;
    captionLayout->addWidget(signInputCaptionLabel);
    captionLayout->addWidget(swapModeButton);
    captionLayout->addWidget(signOutputCaptionLabel);

    inputOutputLayout = new QHBoxLayout;
    inputOutputLayout->addWidget(inputTextEdit);
    inputOutputLayout->addWidget(resultTranslatorLabel);

    mainLayout = new QVBoxLayout(this);
    mainLayout->addLayout(menuLayout);
    mainLayout->addLayout(captionLayout);
    mainLayout->addLayout(inputOutputLayout);

    swapInputType();

    morseTranslator = new MorseTranslator(this);
    QObject::connect(morseTranslator, &MorseTranslator::changedFilePath, [=](QString path){
        // tool tip save full path to file, on label show only name file
        filePathLabel->setToolTip(path);
        QString name;
        for (int i = 0; i < path.length(); i++){
            if (path[i] == '/')
                name = "";
            else
                name += path[i];
        }
        filePathLabel->setText(name);
    });
    QObject::connect(saveToFileButton, &QPushButton::clicked, [=]()
    {
        morseTranslator->saveToFile(resultTranslatorLabel->text(), filePathLabel->toolTip());
    });
    QObject::connect(inputTextEdit, &QTextEdit::textChanged, [=]()
    {
        validatingInput();
        // real time translate
        resultTranslatorLabel->setText(morseTranslator->translate(inputTextEdit->toPlainText(), inputType));
    });
}

void MainWidget::swapInputType()
{
    clear();
    inputType == InputType::MorseCode
            ? inputType = InputType::SimpleText
            : inputType = InputType::MorseCode;
    if (inputType == InputType::MorseCode){
        signInputCaptionLabel->setText(morseCaption);
        signOutputCaptionLabel->setText(latinCaption);
    }
    else {
        signInputCaptionLabel->setText(latinCaption);
        signOutputCaptionLabel->setText(morseCaption);
    }
    inputTextEdit->setFocus();
}

void MainWidget::validatingInput()
{
    QString result("");
    QString inputText = inputTextEdit->toPlainText();
    int spaceCount(0);

    // validating text by morse code
    if (inputType == InputType::MorseCode){
        for (int i = 0; i < inputText.length(); i++){
            if (inputText[i] == ' '){
                spaceCount++;
                if (spaceCount <= 2)
                    result += inputText[i];
            }
            else if (inputText[i] == '-' || inputText[i] == '.'){
                result += inputText[i];
                spaceCount = 0;
            }
        }
    }
    // validating latin text
    else {
        for (int i = 0; i < inputText.length(); i++){
            if (inputText[i] == ' '){
                spaceCount++;
                if (spaceCount <= 1)
                    result += inputText[i];
            }
            else if ((inputText[i] >= 'A' && inputText[i] <= 'Z')
                  || (inputText[i] >= 'a' && inputText[i] <= 'z')
                  || (inputText[i] >= '0' && inputText[i] <= '9')){
                result += inputText[i];
                spaceCount = 0;
            }
        }
    }

    // set valid text
    if (inputText != result){
        inputTextEdit->setText(result);
        inputTextEdit->moveCursor(QTextCursor::End);
    }
}

void MainWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Tab)
        swapInputType();
}

void MainWidget::clear()
{
    inputTextEdit->clear();
    filePathLabel->clear();
    resultTranslatorLabel->clear();
}

void MainWidget::openFileDialog()
{
    QFileDialog dialog(this, "Open file", QDir::currentPath(), "*.txt");
    QObject::connect(&dialog, &QFileDialog::fileSelected, [=](QString fileName)
    {
        // clear();
        auto res = morseTranslator->readFromFile(fileName);
        if (res.first){
            QRegExp regExp("^[a-zA-Z0-9 ]+$");
            if (regExp.exactMatch(res.second)){
                if (inputType != InputType::SimpleText)
                    swapInputType();
            }
            else if (inputType != InputType::MorseCode)
                swapInputType();
            inputTextEdit->setText(res.second.split('/').last());
        }
        else {
            QMessageBox errorBox;
            errorBox.setText("Cannot read the file\n" + res.second);
            errorBox.setIcon(QMessageBox::Warning);
            errorBox.exec();
        }
    });
    dialog.exec();
}
