#include "morsetranslator.h"

MorseTranslator::MorseTranslator(QObject *parent) : QObject(parent)
{
    fillMorseCipher();
}

void MorseTranslator::fillMorseCipher()
{
    morseCipher.clear();
    morseCipher['a'] = ".-";
    morseCipher['b'] = "-...";
    morseCipher['c'] = "-.-.";
    morseCipher['d'] = "-..";
    morseCipher['e'] = ".";
    morseCipher['f'] = "..-.";
    morseCipher['g'] = "--.";
    morseCipher['h'] = "....";
    morseCipher['i'] = "..";
    morseCipher['j'] = ".---";
    morseCipher['k'] = "-.-";
    morseCipher['l'] = ".-..";
    morseCipher['m'] = "--";
    morseCipher['n'] = "-.";
    morseCipher['o'] = "---";
    morseCipher['p'] = ".--.";
    morseCipher['q'] = "--.-";
    morseCipher['r'] = ".-.";
    morseCipher['s'] = "...";
    morseCipher['t'] = "-";
    morseCipher['u'] = "..-";
    morseCipher['v'] = "...-";
    morseCipher['w'] = ".--";
    morseCipher['x'] = "-..-";
    morseCipher['y'] = "-.--";
    morseCipher['z'] = "--..";
    morseCipher['0'] = "-----";
    morseCipher['1'] = ".----";
    morseCipher['2'] = "..---";
    morseCipher['3'] = "...--";
    morseCipher['4'] = "....-";
    morseCipher['5'] = ".....";
    morseCipher['6'] = "-....";
    morseCipher['7'] = "--...";
    morseCipher['8'] = "---..";
    morseCipher['9'] = "----.";
}

std::pair<bool, QString> MorseTranslator::readFromFile(QString filePath)
{
    QFile file(filePath);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)){
        QTextStream stream(&file);
        QString ret = stream.readAll();
        file.close();
        emit changedFilePath(filePath);
        return std::make_pair(true, ret);
    }
    return std::make_pair(false, file.errorString());
}

void MorseTranslator::saveToFile(QString text, QString filePath)
{
    if (text == "")
        return;
    if (filePath == "")
        filePath = QDir::current().absoluteFilePath("Morse_"
                                                    + QDateTime::currentDateTime().toString("dd.MM.yy_hh:mm::ss")
                                                    + ".txt");
    QFile file(filePath);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text)){
        QTextStream stream(&file);
        stream << text;
        file.close();
        emit changedFilePath(filePath);
    }
}

QString MorseTranslator::translate(QString text, InputType type)
{
    QString ret("");
    // convert from morse code to latin text
    if (type == InputType::MorseCode){
        QString currentPath("");
        for (int i = 0; i < text.length(); i++){
            if (text[i] != ' '){
                currentPath += text[i];
                if (i != text.length() - 1)
                    continue;
            }
            // end of word
            if (currentPath == "")
                ret += ' ';
            // find morse code equal currentPath
            else {
                for (auto i = morseCipher.begin(); i != morseCipher.end(); ++i)
                    if (i.value() == currentPath){
                        ret += i.key();
                        break;
                    }
                currentPath = "";
            }
        }
    }
    // convert from latin text to morse
    else {
        text = text.toLower();
        for (int i = 0; i < text.length(); i++){
            if (text[i] == ' ')
                ret += ' ';
            else
                ret += morseCipher.value(text[i]);
            if (i + 1 < text.length())
                ret += ' ';
        }
    }
    return ret;
}
