#include <QtWidgets/QApplication>

#include "mainwidget.h"

#include "test/test_morsetranslator.h"

int main(int argc, char** argv){
    QApplication app(argc, argv);

    MainWidget *wgt = new MainWidget;
    wgt->setWindowTitle("Morse translator");
    wgt->setFixedHeight(wgt->sizeHint().height());
    wgt->setMinimumWidth(650);
    wgt->show();

    Test_MorseTranslator testMorse;
    QTest::qExec(&testMorse);

    return app.exec();
}
