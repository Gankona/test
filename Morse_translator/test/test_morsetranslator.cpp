#include "test_morsetranslator.h"

Test_MorseTranslator::Test_MorseTranslator(QObject *parent)
    : QObject(parent){}

void Test_MorseTranslator::checkTranslator()
{
    MorseTranslator morse;
    QCOMPARE(morse.translate("sos", InputType::SimpleText), QString("... --- ..."));
    QCOMPARE(morse.translate("... --- ...", InputType::MorseCode), QString("sos"));
    QCOMPARE(morse.translate("... --- ...  .... . .-.. .--.", InputType::MorseCode), QString("sos help"));
}
