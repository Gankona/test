#ifndef TEST_MORSETRANSLATOR_H
#define TEST_MORSETRANSLATOR_H

#include <QtCore>
#include <QtGui>
#include <QtTest>

#include "morsetranslator.h"

class Test_MorseTranslator : public QObject
{
    Q_OBJECT
public:
    explicit Test_MorseTranslator(QObject *parent = 0);

private slots:
    void checkTranslator();
};

#endif // TEST_MORSETRANSLATOR_H
