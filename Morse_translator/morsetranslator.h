#ifndef MORSETRANSLATOR_H
#define MORSETRANSLATOR_H

#include <QtCore>

enum class InputType {
    SimpleText = 0,
    MorseCode = 1,
};

class MorseTranslator : public QObject
{
    Q_OBJECT
private:
    QMap <QChar/*character*/, QString/*morse cipher*/> morseCipher;
    inline void fillMorseCipher();

public:
    explicit MorseTranslator(QObject *parent = nullptr);

    std::pair<bool, QString> readFromFile(QString filePath);
    void saveToFile(QString text, QString filePath = "");
    QString translate(QString text, InputType type);

signals:
    void changedFilePath(QString filePath);
};

#endif // MORSETRANSLATOR_H
