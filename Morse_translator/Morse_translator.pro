QT += widgets
QT += testlib
QT += gui

CONFIG += c++14

SOURCES += \
    main.cpp \
    mainwidget.cpp \
    morsetranslator.cpp \
    test/test_morsetranslator.cpp

HEADERS += \
    mainwidget.h \
    morsetranslator.h \
    test/test_morsetranslator.h
