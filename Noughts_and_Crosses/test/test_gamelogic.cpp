#include "test_gamelogic.h"

Test_GameLogic::Test_GameLogic(QObject *parent)
    : QObject(parent)
{}

void Test_GameLogic::testCheckVictoryByFigure(Figure figure)
{
    GameLogic logic;
    logic.clear();
    // test win
    for (int i = 0; i < 3; i++){
        // horizontal test
        for (int j = 0; j < 3; j++)
            logic.setFigure(figure, i*3 + j);
        QVERIFY(logic.checkForVictory());
        logic.clear();
        // vertical test
        for (int j = 0; j < 3; j++)
            logic.setFigure(figure, i + j*3);
        QVERIFY(logic.checkForVictory());
        logic.clear();
        // diagonal test
        logic.setFigure(figure, 0);
        logic.setFigure(figure, 4);
        logic.setFigure(figure, 8);
        QVERIFY(logic.checkForVictory());
        logic.clear();

        logic.setFigure(figure, 2);
        logic.setFigure(figure, 4);
        logic.setFigure(figure, 6);
        QVERIFY(logic.checkForVictory());
        logic.clear();
    }
}

void Test_GameLogic::testCheckVictory()
{
    testCheckVictoryByFigure(Figure::Cross);
    testCheckVictoryByFigure(Figure::Nought);
}

void Test_GameLogic::testSetFigure()
{
    GameLogic logic;
    logic.clear();
    for (int i = 0; i < 8; i++){
        QVERIFY(logic.setFigure(Figure::Cross, i));
        QVERIFY(! logic.setFigure(Figure::Cross, i));
    }
    QVERIFY(! logic.setFigure(Figure::Nought, 7));
    QVERIFY(logic.setFigure(Figure::Nought, 8));
}
