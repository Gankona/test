#ifndef TEST_GAMELOGIC_H
#define TEST_GAMELOGIC_H

#include <QtCore>
#include <QtGui>
#include <QtTest>

#include "C++/gamelogic.h"

class Test_GameLogic : public QObject
{
    Q_OBJECT
private:
    inline void testCheckVictoryByFigure(Figure figure);

public:
    explicit Test_GameLogic(QObject *parent = 0);

private slots:
    void testSetFigure();
    void testCheckVictory();
};

#endif // TEST_GAMELOGIC_H
