import QtQml 2.2
import QtQuick 2.5
import QtQuick.Controls 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import PlayerInfo 1.0

import "qrc:/QML"

Item {
    id: mainWindow
    anchors.fill: parent

    property string lineColor: "navy"

    function clearDesk(){
        desk.clearDesk()
    }

    function setTurn(playerNumber){
        if (playerNumber === 1)
            turnFrame.state = '';
        else
            turnFrame.state = 'right';
    }

    function showMessage(str){
        messaeBox.visible = true
        messageText.text = str
    }

    function hideMessage(){
        messaeBox.visible = false
    }

    // Upper panel, it is information about a series of matches
    // and button to clear the counters
    Rectangle {
        id: infoPanel
        anchors.top: parent.top
        anchors.bottom: desk.top
        anchors.left: parent.left
        anchors.right: parent.right

        // information about player1
        Rectangle {
            id: player1_frame
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            width: (parent.width - parent.height) / 2

            Text {
                text: player1.name
                font.pointSize: 13
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                height: parent.height / 2
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            Text {
                id: player1_score
                text: "score " + player1.score
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                height: parent.height / 2
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        // information about player2
        Rectangle {
            id: player2_frame
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            width: (parent.width - parent.height) / 2

            Text {
                text: player2.name
                font.pointSize: 13
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.top: parent.top
                height: parent.height / 2
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }

            Text {
                id: player2_score
                text: "score: " + player2.score
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                height: parent.height / 2
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
        }

        // floating rectangle to show whose turn
        Rectangle {
            id: turnFrame
            x: 0
            color: "green"
            opacity: 0.3
            height: player1_frame.height
            width: player1_frame.width
            antialiasing: true

            states : State {
                name: "right"
                PropertyChanges { target: turnFrame; x: mainWindow.width - turnFrame.width; color: "green" }
            }

            transitions: Transition {
                NumberAnimation { properties: "x"; easing.type: Easing.InOutSine; duration: 350 }
            }
        }

        // reset score and clear desk
        Button {
            id: resetButton

            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: player1_frame.right
            anchors.right: player2_frame.left

            onClicked: Qt_match.resetProgress()

            Image {
                anchors.fill: parent
                source: "qrc:/reset.png"
                sourceSize.width: parent.width
                sourceSize.height: parent.height
            }
        }
    }

    PlayDesk {
        id: desk

        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        width: parent.width
        height: parent.width
    }

    // information about win or dead heat
    Rectangle {
        id: messaeBox
        anchors.fill: parent
        visible: false
        color: "navy"
        opacity: 0.90

        Text {
            id: messageText
            color: "white"
            font.pointSize: 22
            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }
}
