import QtQuick 2.0
import "qrc:/QML"

Rectangle {
    function clearDesk(){
        for (var i = 0; i < 9; i++){
            var rect = deskRepeater.itemAt(i);
            // i dont know how find only Cross child,
            // and find length of children, first always was MouseArea
            if (rect.children.length >= 2){
                var childFigure = rect.childAt(rect.width/2, rect.height/2)
                if (childFigure !== rect.mouseArea)
                    childFigure.destroy();
            }
        }
    }

    // the panel on which the size of the field of play 3 on 3
    Rectangle {
        id: squareRect
        anchors.centerIn: parent
        width: parent.width*4/5
        height: parent.height*4/5
        color: lineColor

        Grid {
            columns: 3
            rows: 3
            spacing: 6
            Repeater {
                id: deskRepeater
                model: 9

                Rectangle {
                    width: squareRect.width/3 - 4
                    height: squareRect.width/3 - 4

                    // draw nought
                    function setNought(index){
                        Qt.createComponent("Nought.qml").createObject(deskRepeater.itemAt(index))
                    }

                    // draw cross
                    function setCross(index){
                        Qt.createComponent("Cross.qml").createObject(deskRepeater.itemAt(index))
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: Qt_match.chooseSquare(index, parent)
                    }
                }
            }
        }
    }
}
