import QtQuick 2.0
import "qrc:/QML"

Rectangle {
    radius: parent.width*2/7
    width: 2 * radius
    height: 2 * radius
    anchors.centerIn: parent
    border.color: lineColor
    border.width: 3
}
