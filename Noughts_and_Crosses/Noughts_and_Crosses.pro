QT += qml
QT += quick
QT += testlib
QT += gui

CONFIG += c++14
TARGET = "Noughts_and_Crosses"

RESOURCES += \
    resourses.qrc

SOURCES += \
    main.cpp \
    C++/gamelogic.cpp \
    C++/matchmanager.cpp \
    test/test_gamelogic.cpp

HEADERS += \
    C++/gamelogic.h \
    C++/matchmanager.h \
    C++/playerinfo.h \
    test/test_gamelogic.h

OTHER_FILES += \
    QML/*.qml
