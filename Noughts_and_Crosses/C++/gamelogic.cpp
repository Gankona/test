#include "gamelogic.h"

GameLogic::GameLogic(QObject *parent) : QObject(parent)
{
    clear();
}

bool GameLogic::isSquareFree(int index)
{
    return figureMas[index/3][index%3] == Figure::None;
}

void GameLogic::clear()
{
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            figureMas[i][j] = Figure::None;
}

bool GameLogic::setFigure(Figure figure, int index)
{
    // test is it possible to like in a square
    if (! isSquareFree(index))
        return false;
    figureMas[index/3][index%3] = figure;
    return true;
}

bool GameLogic::checkForVictory()
{
    // test dead heat
    int noneFigure(0);
    for (int i = 0; i < 3; i++)
        for (int j = 0; j < 3; j++)
            if (figureMas[i][j] == Figure::None)
                noneFigure++;
    if (noneFigure == 0){
        emit draw();
        return true;
    }

    int crossMatchD1(0);
    int crossMatchD2(0);
    int noughtMatchD1(0);
    int noughtMatchD2(0);

    for (int i = 0; i < 3; i++){
        int crossMatchH(0);
        int crossMatchV(0);
        int noughtMatchH(0);
        int noughtMatchV(0);

        for (int j = 0; j < 3; j++){
            // vertical
            if (figureMas[i][j] == Figure::Cross)
                crossMatchV++;
            else if (figureMas[i][j] == Figure::Nought)
                noughtMatchV++;

            // horiz
            if (figureMas[j][i] == Figure::Cross)
                crossMatchH++;
            else if (figureMas[j][i] == Figure::Nought)
                noughtMatchH++;
        }
        if (crossMatchV == 3 || crossMatchH == 3){
            emit victory(Figure::Cross);
            return true;
        }
        if (noughtMatchV == 3 || noughtMatchH == 3){
            emit victory(Figure::Nought);
            return true;
        }

        // diagonals
        if (figureMas[i][i] == Figure::Cross)
            crossMatchD1++;
        else if (figureMas[i][i] == Figure::Nought)
            noughtMatchD1++;

        if (figureMas[i][2-i] == Figure::Cross)
            crossMatchD2++;
        else if (figureMas[i][2-i] == Figure::Nought)
            noughtMatchD2++;
    }

    if (crossMatchD1 == 3 || crossMatchD2 == 3){
        emit victory(Figure::Cross);
        return true;
    }
    else if (noughtMatchD1 == 3 || noughtMatchD2 == 3){
        emit victory(Figure::Nought);
        return true;
    }
    return false;
}
