#include "matchmanager.h"

MatchManager::MatchManager(QObject *parent) : QObject(parent)
{
    qmlRegisterType<PlayerInfo>("PlayerInfo", 1, 0, "PlayerInfo");

    gameLogic = new GameLogic(this);
    player1 = new PlayerInfo("Player 1", Figure::Cross);
    player2 = new PlayerInfo("Player 2", Figure::Nought);
    currentPlayer = player1;

    view = new QQuickView();
    view->rootContext()->setContextProperty("Qt_match", this);
    view->rootContext()->setContextProperty("player1", player1);
    view->rootContext()->setContextProperty("player2", player2);
    view->setSource(QUrl("qrc:/QML/main.qml"));
    view->resize(365, 400);
    view->show();

    QObject::connect(gameLogic, &GameLogic::victory,  this, &MatchManager::victory);
    QObject::connect(gameLogic, &GameLogic::draw, this, &MatchManager::draw);
}

void MatchManager::chooseSquare(int index, QObject *obj)
{
    // if clicked was on used area, ignore it
    if (! gameLogic->setFigure(currentPlayer->getFigure(), index))
        return;

    // draw turn on desk and swap players
    if (currentPlayer->getFigure() == Figure::Cross)
        QMetaObject::invokeMethod(obj, "setCross", Q_ARG(QVariant, index));
    else
        QMetaObject::invokeMethod(obj, "setNought", Q_ARG(QVariant, index));
    if (currentPlayer == player1){
        currentPlayer = player2;
        QMetaObject::invokeMethod(view->rootObject(), "setTurn", Q_ARG(QVariant, 2));
    }
    else {
        currentPlayer = player1;
        QMetaObject::invokeMethod(view->rootObject(), "setTurn", Q_ARG(QVariant, 1));
    }

    // check game for victory
    gameLogic->checkForVictory();
}

void MatchManager::resetProgress()
{
    gameLogic->clear();
    player1->setScore(0);
    player2->setScore(0);
    QMetaObject::invokeMethod(view->rootObject(), "clearDesk");
}

void MatchManager::victory(Figure figure)
{
    PlayerInfo* winPlayer;
    // update score and set first turn to loser
    if (player1->getFigure() == figure){
        player1->setScore(player1->score() + 1);
        winPlayer = player1;
        currentPlayer = player2;
        QMetaObject::invokeMethod(view->rootObject(), "setTurn", Q_ARG(QVariant, 2));
    }
    else {
        player2->setScore(player2->score() + 1);
        winPlayer = player2;
        currentPlayer = player1;
        QMetaObject::invokeMethod(view->rootObject(), "setTurn", Q_ARG(QVariant, 1));
    }

    // show message whose win, after 3 seconds hide it
    QMetaObject::invokeMethod(view->rootObject(), "showMessage", Q_ARG(QVariant, winPlayer->name() + "\nWIN!"));
    QTimer::singleShot(3000, this, [=](){
        QMetaObject::invokeMethod(view->rootObject(), "hideMessage");
        gameLogic->clear();
        QMetaObject::invokeMethod(view->rootObject(), "clearDesk");
    });
}

void MatchManager::draw()
{
    // show message anout dead heat, after 3 seconds hide it
    QMetaObject::invokeMethod(view->rootObject(), "showMessage", Q_ARG(QVariant, "Draw"));
    QTimer::singleShot(3000, this, [=](){
        QMetaObject::invokeMethod(view->rootObject(), "hideMessage");
        gameLogic->clear();
        QMetaObject::invokeMethod(view->rootObject(), "clearDesk");
    });
}
