#ifndef PLAYERINFO_H
#define PLAYERINFO_H

#include <QtCore>

enum class Figure {
    Nought = 0,
    Cross = 1,
    None = 2,
};

class PlayerInfo : public QObject
{
    Q_OBJECT
private:
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(int score READ score WRITE setScore NOTIFY scoreChanged)
    QString m_name;
    Figure figure;
    int m_score;

public:
    PlayerInfo() = default;
    PlayerInfo(QString name, Figure figure, int score = 0)
        : m_name(name), figure(figure), m_score(score) {}

    int score() const { return m_score; }

    Figure getFigure() const { return figure; }

    void setScore(int score) {
        m_score = score;
        emit scoreChanged(score);
    }

    QString name() const {
        if (figure == Figure::Cross)
            return m_name + " - o";
        return m_name + " - x";
    }

signals:
    void scoreChanged(int score);
    void nameChanged(QString name);
};

#endif // PLAYERINFO_H
