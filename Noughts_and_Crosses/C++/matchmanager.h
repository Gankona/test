#ifndef MATCHMANAGER_H
#define MATCHMANAGER_H

#include <QtCore>
#include <QtGui>
#include <QQuickView>
#include <QQmlContext>
#include <QQuickItem>

#include "playerinfo.h"
#include "gamelogic.h"

class MatchManager : public QObject
{
    Q_OBJECT
private:
    PlayerInfo *player1;
    PlayerInfo *player2;
    PlayerInfo *currentPlayer;

    QQuickView *view;
    GameLogic *gameLogic;

    void draw();
    void victory(Figure figure);

public:
    explicit MatchManager(QObject *parent = nullptr);

    Q_INVOKABLE void chooseSquare(int index, QObject *obj);
    Q_INVOKABLE void resetProgress();
};

#endif // MATCHMANAGER_H
