#include <QGuiApplication>
#include "C++/matchmanager.h"
#include "test/test_gamelogic.h"

int main(int argc, char** argv){
    QGuiApplication app(argc, argv);
    app.setWindowIcon(QIcon(":/icon.png"));

    MatchManager *manager = new MatchManager;

    Test_GameLogic testGameLogic;
    QTest::qExec(&testGameLogic);

    return app.exec();
}
